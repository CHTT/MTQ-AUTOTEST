import requests,pytest
from config import globalconfig
import datetime
from common.read_yaml import OperationYaml

# 从配置文件读取测试数据路径
baseurl = globalconfig.business_url

class Test_photolist():

    @pytest.mark.photolist
    def test_photolist(self):
        '''檬太奇个人中心查看我上传的照片列表'''
        url = baseurl +'/imageCollection/list'

        payload ={
            'unionId':'oYnHqs82GD8B6ceiztzIi3MkUbBU'
        }

        r = requests.get(url,params=payload)
        print(r.json())
        msg = r.json()['message']
        assert msg == '操作成功'

if __name__ == '__main__':
    Test_photolist().test_photolist()