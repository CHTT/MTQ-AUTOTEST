import requests,pytest
from config import globalconfig
from common.read_yaml import OperationYaml

from operate.interface_operate import interface_operate

# 从配置文件读取测试数据路径
baseurl = globalconfig.business_url


class Test_movement_create():


    @pytest.mark.movement_create
    def test_movement_create(self):
        '''檬太奇创建动态'''
        picHeight,picId,picUrl,picWidth,thumbnailUrl = interface_operate().upload_cfile()
        url = baseurl +'/movement/add'

        headers = {'content-type':'application/json'}
        payload = {
            "content":"接口自动化测试创建动态",
            "imgUrlsList":[{
                "picHeight":"%s"%picHeight,
                "picId":"%s"%picId,
                "picUrl":"%s"%picUrl,
                "picWidth":"%s"%picWidth,
                "thumbnailUrl":"%s"%thumbnailUrl}],
            "tagsList":[],
            "unionId":"oYnHqs82GD8B6ceiztzIi3MkUbBU",
            "discoverId": "0"
        }

        r = requests.post(url= url, json=payload, headers = headers)
        print(r.json())

        rsl = r.json()['code']
        assert rsl == 200


if __name__ == '__main__':
    Test_movement_create().test_movement_create()