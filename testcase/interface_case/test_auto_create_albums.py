import requests,pytest,os
from config import globalconfig
from common.read_yaml import OperationYaml

# 从配置文件读取测试数据路径
datapath = globalconfig.interface_data

yaml = os.path.join(datapath,'auto_create_albums.yml')

# 定义参数名称
args_item = 'url,send_data,resultstr'

# 读取yaml文件加载测试数据
test_data, case_desc = OperationYaml(yaml).load_file

@pytest.mark.parametrize(args_item,test_data,ids=case_desc)
class Test_auto_create_albums():

    @pytest.mark.auto_create_albums
    def test_auto_create_albums(self,url,send_data,resultstr):
        '''用户在图形图像进行DIY后，调用接口自动创建相册'''

        payload = send_data

        r = requests.post(url,data = payload)
        print(r.json())
        rsl = r.json()['message']

        assert rsl in resultstr['resultstr']


if __name__ == '__main__':
    Test_auto_create_albums().test_auto_create_albums()