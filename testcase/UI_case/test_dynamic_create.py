import pytest, os
from time import sleep
from operate.business_operate import businessoperate
from common.read_yaml import OperationYaml
from config import globalconfig
from page_object.dynamic_management.dynamic_create.dynamic_create import dynamic_create
from common.upload_photo import upload_photo


# 从配置文件读取测试数据路径
datapath = globalconfig.UI_data
yaml = os.path.join(datapath, 'dynamic_create.yml')

# 定义参数名称
args_item = 'url,send_data,resultstr'

# 读取yaml文件加载测试数据
test_data, case_desc = OperationYaml(yaml).load_file

@pytest.mark.parametrize(args_item,test_data,ids=case_desc)
class Test_dynamic():

    @pytest.mark.dynamic_create
    def test_dynamic_create(self, browser, baseurl, url, send_data, resultstr):
        '''
        测试创建动态
        '''
        #登录
        page = businessoperate().login_success(browser, baseurl)
        #最大化窗口
        browser.maximize_window()
        #跳转到测试路径
        page.get(url)
        sleep(2)
        #将浏览器驱动给测试页面
        page2 = dynamic_create(browser)
        sleep(2)
        #点击马甲选择框
        page2.waistcoat_box.click()
        sleep(2)
        # 选择马甲
        page2.waistcoat_name.click()
        sleep(2)
        # 点击专题选择框
        page2.subject_box.click()
        sleep(2)
        # 选择专题
        page2.subject_name.click()
        sleep(2)
        # 输入正文
        page2.signature.send_keys(send_data['signature'])
        sleep(2)
        #点击上传图片按钮
        page2.photo_btn.click()
        sleep(2)
        # 上传图片
        upload_photo()
        sleep(2)
        # 点击发布
        page2.publish_btn.click()
        sleep(2)
        #发布后取浏览器的URL
        url = browser.current_url
        assert url == resultstr['resultstr']


if __name__ == '__main__':
    pytest.main(['-q',r'D:\MTQ\testcase\UI_case\test_dynamic_create.py'])
