
# 说明：
# 1、用例创建原则，测试文件名必须以“test_”开头，测试类必须以“Test”开头（注意首字母大写），测试方法以test_开头。
# 2、用例运行参数,更多运行参数请参照pytest --help
# *  -s 指定运行目录或文件，例: -s  ./testcase/ ,  -s  /testcase/test_demo.py
# *  --html= **  指定测试报告目录及文件名。
# *  --self-contained-html 表示创建独立的测试报告。
# *  --tests-per-worker auto  用于指定线程数，auto表示自动分配
# *  --reruns=1  失败重跑次数设置，主要为了防止因网络原因造成的用例失败
# *  --lf  在运行run_test.py后有失败的用例，再运行'pytest --lf'命令直接运行失败的用例

import pytest
import time
from common.Shell import Shell
from common.SendEmail import newReport,sendReport
from config import globalconfig
from common.Log import logger



#从配置文件获取测试用例路径和报告路径
test_dir = globalconfig.TestCase_path
#print(test_dir)


report_path = globalconfig.report_path

#运行参数
'''
-x:有错误时停止执行用例，用于调试阶段
-s:用于关闭捕捉，从而输出打印信息
-q:减少测试运行冗长
-v:增加测试用例冗长
-m:只运行标记关键字的用例
-collect-only:只收集用例不执行测试
--tb=style：这个选项用于对失败输出信息的显示方式，输出的信息一般包括 1. 失败出现在哪一行2.是什么失败 3.怎么失败的 这三要素是信息追溯
我们常用的style 有 short，no，line
'''
arguments = '-q'

# 当前时间
str_time = time.strftime("%Y-%m-%d %H-%M-%S")
# 报告名称
name = str_time + '-report.html'
# pytest-html报告
html_report = "--html={}/{}".format(report_path,name)
# 报告融合CSS样式
css = '--self-contained-html'
# 设置运行线程数
worker1 = '--tests-per-worker=auto'
worker2 = '--workers=1'
# 设置失败重跑的次数
reload = '--reruns=1'
# 收集报错信息
error = '--tb=line'


if __name__ == "__main__":

    # 构建一个shell运行脚本
    # shell = Shell()

    #运行测试目录所有测试用例
    args = [arguments,test_dir,html_report,css,reload]
    #匹配标记的关键字运行测试
    args1 = ['-m','get_room_list',test_dir,html_report,css,reload,error]

    pytest.main(args)

    # 收集运行日志
    logger.info('-+-+-+-+-+-+-+-+-info-+-+-+-+-+-+-+-+-')
    logger.warning('-+-+-+-+-+-+-+-+-warning-+-+-+-+-+-+-+-+-')
    logger.error('-+-+-+-+-+-+-+-+-error-+-+-+-+-+-+-+-+-')

    # 发送最新报告
    new_report = newReport(report_path)
    sendReport(new_report)

#这是在dev分支修改的内容！！！！#这是在dev分支修改的内容！！！！#这是在dev分支修改的内容！！！！