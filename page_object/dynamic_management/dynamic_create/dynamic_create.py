from poium import Page,PageElement

# 檬太奇后台-动态管理-新建动态页面
class dynamic_create(Page):
    waistcoat_box = PageElement(css='.ivu-select-input', describe="请选择马甲")
    waistcoat_name = PageElement(xpath="//div[@id='app']/div//div[@class='ivu-row']/div[@class='ivu-col ivu-col-span-17']//form[@class='ivu-form ivu-form-label-right']/div[1]/div[@class='ivu-form-item-content']/div/div[@class='ivu-select-dropdown']/ul[@class='ivu-select-dropdown-list']/li[38]", describe="选定马甲名字")
    subject_box = PageElement(css='.ivu-select-placeholder', describe="请选择专题")
    subject_name = PageElement(xpath="//div[@id='app']/div//div[@class='ivu-row']/div[@class='ivu-col ivu-col-span-17']/div[@class='ivu-card']//form[@class='ivu-form ivu-form-label-right']/div[2]/div[@class='ivu-form-item-content']/div/div[@class='ivu-select-dropdown']/ul[@class='ivu-select-dropdown-list']//span[.='不负春光，不负卿']", describe="不负春光，不负卿")
    signature = PageElement(css='.ivu-input', describe="正文")
    photo_btn = PageElement(css='.ivu-icon-ios-camera', describe="点击上传图片按钮")
    publish_btn = PageElement(css='.ivu-btn-primary span', describe="发布")
